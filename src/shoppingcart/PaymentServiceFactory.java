/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package shoppingcart;

/**
 *
 * @author SHUBHAM PADHYA
 */
public class PaymentServiceFactory {
  private static PaymentServiceFactory factory;
    
    private PaymentServiceFactory(){}
    public static PaymentServiceFactory getInstance(){
        
     if (factory==null)
         factory = new PaymentServiceFactory();
     return factory;
        
    }
     static PaymentService getPaymentService(PaymentServiceType type){
     switch(type){
         case DEBIT -> {
             return new DebitPaymentService();
          }   
            case CREDIT -> {
                return new CreditPaymentService();
          }
     }
     
      
         
        
     return null;
    }
    
}
