/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Main.java to edit this template
 */
package shoppingcart;

/**
 *
 * @author SHUBHAM PADHYA
 */
public class ShoppingCart {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
       PaymentServiceFactory factory = PaymentServiceFactory.getInstance();
        PaymentService creditService = (PaymentService) factory.getPaymentService(PaymentServiceType.CREDIT);
        PaymentService debitService = (PaymentService) factory.getPaymentService(PaymentServiceType.DEBIT);
        
        Cart cart = new Cart();
        cart.addProduct( new Product("shirt", 50));
        cart.addProduct(new Product("pants" , 60));
        
        cart.setPaymentService(debitService);
        cart.payCart();
        
        cart.setPaymentService(creditService);
        cart.payCart();
    }

}
