/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package shoppingcart;

import java.util.ArrayList;

/**
 *
 * @author SHUBHAM PADHYA
 */
public class Cart {
       private ArrayList<Product> products;
    private PaymentService service;
    
        public Cart(){
         products = new ArrayList<Product>();   
            
            
            
        }
    
    public void setPaymentService(PaymentService service){
        this.service = service;
        
    }
    public void addProduct(Product product){
        
    products.add(product);    
    
    }
    public void payCart(){
       double totalcost =0;
       
       for(int i =0; i<products.size(); i++){
           
           totalcost+=products.get(i).getprice();
           
       }
        
        service.processPayment(totalcost);
        
    }
    
    
}
