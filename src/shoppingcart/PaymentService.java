/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package shoppingcart;

/**
 *
 * @author SHUBHAM PADHYA
 */
public abstract class PaymentService {
        public abstract void processPayment(double amount);
    
}
